#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

typedef struct List
{
    int daysLeft;
    struct List *next;
}List;

void addToList(List *list, int value)
{
    List *current = list;
    while(current->next != NULL)
    {
        current = current->next;
    }

    current->next = malloc(sizeof(List));
    current->next->daysLeft = value;
    current->next->next = NULL;
}

void freeList(List *list)
{
    List *current = list;
    List *next;
    while(current != NULL)
    {
        next = current->next;
        free(current);
        current = next;
    }
}

void printList(List *list)
{

    List *current = list->next;
    while(current != NULL)
    {
        printf("%d ", current->daysLeft);
        current = current->next;
    }
    printf("\n");
}

int countNodes(List *list)
{
    int acc = 0;
    List *current = list->next;
    while(current != NULL)
    {
        acc++;
        current = current->next;
    }
    return acc;
}

int main()
{
    FILE *input = fopen("input.txt", "r");
    if(input == NULL)
    {
        printf("Could not open file\n");
        exit(1);
    }

    List fishes;
    fishes.next = NULL;
    char ch;
    while((ch = fgetc(input)) != EOF)
    {
       addToList(&fishes, atoi(&ch));
       fgetc(input);
    }
    fclose(input);
    /* printList(&fishes); */
    const int days = 18;
    for(int i = 0; i < days; ++i)
    {
        List *current = fishes.next;
        while(current != NULL)
        {
            if(current->daysLeft == 0)
            {
                current->daysLeft = 7;
                addToList(current, 9);
            }
            current = current->next;
        }

        current = fishes.next;
        while(current != NULL)
        {
            current->daysLeft--;
            current = current->next;
        }
    /* printList(&fishes); */
    }
    printf("%d\n", countNodes(&fishes));
    return 0;
}
