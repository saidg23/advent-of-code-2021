#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *input = fopen("input.txt", "r");
    if(input == NULL)
    {
        printf("Could not open file");
        exit(1);
    }

    int entryCount = 0;
    char ch;
    while((ch = fgetc(input)) != EOF)
    {
        if(ch == '\n')
            entryCount++;
    }
    rewind(input);

    int *entries = malloc(sizeof(int) * entryCount);
    char entry[50];
    int i = 0;
    int j = 0;
    while((ch = fgetc(input)) != EOF)
    {
        entry[i] = ch;
        if(ch == '\n')
        {
            entry[i] = '\0';
            entries[j] = atoi(entry);
            i = 0;
            j++;
        }
        i++;
    }
    fclose(input);

    int lastSum = entries[0] + entries[1] + entries[2];
    int count = 0;
    for(size_t k = 1; k < entryCount - 4; ++k)
    {
        int sum = entries[k] + entries[k+1] + entries[k+2];
        if(sum > lastSum)
        {
            count++;
        }
        lastSum = sum;
    }

    free(entries);
    printf("%d\n", count);

    return 0;
}
