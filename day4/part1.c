#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

bool checkWin(bool *boardStates)
{
    for(int i = 0; i < 5; ++i)
    {
        if(boardStates[i * 5] && boardStates[i * 5 + 1] && boardStates[i * 5 + 2] && boardStates[i * 5 + 3] && boardStates[i * 5 + 4])
            return true;
    }

    for(int i = 0; i < 5; ++i)
    {
        if(boardStates[i] && boardStates[i + 5] && boardStates[i + 10] && boardStates[i + 15] && boardStates[i + 20])
            return true;
    }
    return false;
}

int main()
{
    FILE *input = fopen("input.txt", "r");
    if(input == NULL)
    {
        printf("Could not open file\n");
        exit(1);
    }

    char ch;
    int moveCount = 0;
    while((ch = fgetc(input)) != '\n')
    {
        if(ch == ',') moveCount++;
    }
    moveCount++;
    fgetc(input);

    int entryCount = 0;
    while((ch = fgetc(input)) != EOF)
    {
        if(ch == '\n')
        {
            ch = fgetc(input);
            if(ch == '\n') entryCount++;
        }
    }
    entryCount++;
    rewind(input);

    int *moves = malloc(sizeof(int) * moveCount);
    int i = 0;
    char buff[3];
    buff[2] = '\0';
    while(true)
    {
        ch = fgetc(input);
        if(ch == '\n')
            break;
        buff[0] = ch;
        ch = fgetc(input);
        if(ch == ',' || ch == '\n')
        {
            buff[1] = '\0';
        }
        else
        {
            buff[1] = ch;
            fgetc(input);
        }
        moves[i] = atoi(buff);
        i++;
    }
    fgetc(input);

    int *boards = malloc(sizeof(int) * 25 * entryCount);
    bool *boardStates = malloc(sizeof(bool) * 25 * entryCount);
    i = 0;
    while(ch != EOF)
    {
        for(int j = 0; j < 25; ++j)
        {
            boardStates[i] = false;
            buff[0] = ch;
            buff[1] = fgetc(input);
            boards[i] = atoi(buff);
            fgetc(input);
            ch = fgetc(input);
            if(ch == '\n') ch = fgetc(input);
            i++;
        }
    }

    int winner;
    int lastMove;
    bool exitloop = false;
    for(int i = 0; i < moveCount; ++i)
    {
        for(int j = 0; j < entryCount; ++j)
        {
            for(int k = 0; k < 25; ++k)
            {
                if(boards[j * 25 + k] == moves[i])
                    boardStates[j * 25 + k] = true;
            }
            if(checkWin(boardStates + j * 25))
            {
                winner = j;
                lastMove = moves[i];
                exitloop = true;
                /* printf("%d\n%d\n", j, lastMove); */
                break;
            }
        }
        if(exitloop) break;
    }

    int sum = 0;
    for(int i = 0; i < 25; ++i)
    {
        if(!boardStates[winner * 25 + i]) sum += boards[winner * 25 + i];
    }

    printf("%d\n", sum * lastMove);

    fclose(input);
    free(moves);
    free(boards);
    free(boardStates);

    return 0;
}
