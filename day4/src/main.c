#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

bool checkWin(bool *boardStates)
{
    for(int i = 0; i < 5; ++i)
    {
        if(boardStates[i * 5] && boardStates[i * 5 + 1] && boardStates[i * 5 + 2] && boardStates[i * 5 + 3] && boardStates[i * 5 + 4])
            return true;
    }

    for(int i = 0; i < 5; ++i)
    {
        if(boardStates[i] && boardStates[i + 5] && boardStates[i + 10] && boardStates[i + 15] && boardStates[i + 20])
            return true;
    }
    return false;
}

int main()
{
    FILE *input = fopen("input.txt", "r");
    if(input == NULL)
    {
        printf("Could not open file\n");
        exit(1);
    }

    char ch;
    int moveCount = 0;
    while((ch = fgetc(input)) != '\n')
    {
        if(ch == ',') moveCount++;
    }
    moveCount++;
    fgetc(input);

    int entryCount = 0;
    while((ch = fgetc(input)) != EOF)
    {
        if(ch == '\n')
        {
            ch = fgetc(input);
            if(ch == '\n') entryCount++;
        }
    }
    entryCount++;
    rewind(input);

    int *moves = malloc(sizeof(int) * moveCount);
    int i = 0;
    char buff[4];
    buff[3] = '\0';
    while(true)
    {
        ch = fgetc(input);
        if(ch == '\n')
            break;
        buff[0] = ch;
        ch = fgetc(input);
        if(ch == ',' || ch == '\n')
        {
            buff[1] = '\0';
            moves[i] = atoi(buff);
            i++;
            continue;
        }
        buff[1] = ch;
        ch = fgetc(input);
        if(ch == ',' || ch == '\n')
        {
            buff[2] = '\0';
            moves[i] = atoi(buff);
            i++;
            continue;
        }
        else
        {
            buff[2] = ch;
            fgetc(input);
            moves[i] = atoi(buff);
            i++;
        }
    }
    fgetc(input);

    int *boards = malloc(sizeof(int) * 25 * entryCount);
    bool *boardStates = malloc(sizeof(bool) * 25 * entryCount);
    i = 0;
    while(ch != EOF)
    {
        boardStates[i] = false;
        buff[0] = ch;
        buff[1] = fgetc(input);
        buff[2] = fgetc(input);
        boards[i] = atoi(buff);
        fgetc(input);
        ch = fgetc(input);
        if(ch == '\n') ch = fgetc(input);
        i++;
    }

    bool *winners = malloc(sizeof(bool) * entryCount);
    for(int i = 0; i < entryCount; ++i)
    {
        winners[i] = false;
    }

    int lastWinner;
    int lastMove;
    bool exitloop = false;
    for(int i = 0; i < moveCount; ++i)
    {
        for(int j = 0; j < entryCount; ++j)
        {
            if(winners[j]) continue;
            for(int k = 0; k < 25; ++k)
            {
                if(boards[j * 25 + k] == moves[i])
                    boardStates[j * 25 + k] = true;
            }
            if(checkWin(boardStates + j * 25))
            {
                winners[j] = true;
                lastWinner = j;
                lastMove = moves[i];
            }
        }
        if(exitloop) break;
    }

    int sum = 0;
    for(int i = 0; i < 25; ++i)
    {
        if(!boardStates[lastWinner * 25 + i]) sum += boards[lastWinner * 25 + i];
    }

    printf("%d\n", lastMove * sum);

    fclose(input);
    free(moves);
    free(boards);
    free(boardStates);
    free(winners);

    return 0;
}
