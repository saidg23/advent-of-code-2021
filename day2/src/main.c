#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *input = fopen("input.txt", "r");
    if(input == NULL)
    {
        printf("Could not open file");
        exit(1);
    }

    char ch;
    char entry[50];
    int i = 0;
    int horizontal = 0;
    int depth = 0;
    int aim = 0;
    while((ch = fgetc(input)) != EOF)
    {
        entry[i] = ch;
        if(ch == '\n')
        {
            entry[i] = '\0';
            char *direction = strtok(entry, " ");
            int quantity = atoi(strtok(NULL, " "));
            if(strcmp(direction, "forward") == 0) { horizontal += quantity ; depth += aim * quantity;}
            else if(strcmp(direction, "down") == 0) { aim += quantity ;}
            else if(strcmp(direction, "up") == 0) { aim -= quantity ;}
            i = 0;
            continue;
        }
        i++;
    }
    fclose(input);
    printf("%d\n", horizontal * depth);

    return 0;
}
