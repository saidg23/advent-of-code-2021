#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../include/stb_image_write.h"

typedef struct Line
{
    int x1;
    int y1;
    int x2;
    int y2;
}Line;

int max(int a, int b)
{
    if(a >= b) return a;
    else return b;
}

int min(int a, int b)
{
    if(a <= b) return a;
    else return b;
}

bool boundCheck(int x, int y, int width, int height)
{
    return x >= 0 && y >= 0 && x < width - 1 && y < height - 1;
}

double magnitude(int x, int y)
{
    return sqrt((double)x * (double)x + (double)y * (double)y);
}

void drawRadius(int *grid, int x, int y, int width, int height)
{
    const int radius = 5;
    for(int i = x - radius; i < x + radius; ++i)
    {
        for(int j = y - radius; j < y + radius; ++j)
        {
            if(boundCheck(i, j, width, height))
            {
                static double mag;
                mag = magnitude(i - x, j - y);
                if(mag < (double)radius)
                {
                    grid[i * width + j] += (int)((double)radius - mag);
                }
            }
        }
    }
}

void drawLine(Line line, int *grid, int width, int height)
{
    int xdir = line.x2 - line.x1;
    if(xdir != 0) xdir = xdir / abs(xdir);

    int ydir = line.y2 - line.y1;
    if(ydir != 0) ydir = ydir / abs(ydir);

    int x = line.x1 - xdir;
    int y = line.y1 - ydir;
    while(x != line.x2 || y != line.y2)
    {
        x += xdir;
        y += ydir;
        drawRadius(grid, x, y, width, height);
    }
}

int main()
{
    FILE *input = fopen("input.txt", "r");
    if(input == NULL)
    {
        printf("Could not open file\n");
        exit(1);
    }

    char ch;
    int entryCount = 0;
    while((ch = fgetc(input)) != EOF)
    {
        if(ch == '\n')
        {
            entryCount++;
        }
    }
    rewind(input);

    Line *lines = malloc(sizeof(Line) * entryCount);

    char buff[100];
    int j = 0;
    for(int i = 0; i < entryCount; ++i)
    {
        while((ch = getc(input)) != '\n')
        {
            buff[j] = ch;
            j++;
        }
        buff[j] = '\0';
        j = 0;
        lines[i].x1 = atoi(strtok(buff, ","));
        lines[i].y1 = atoi(strtok(NULL, " "));
        strtok(NULL, " ");
        lines[i].x2 = atoi(strtok(NULL, ","));
        lines[i].y2 = atoi(strtok(NULL, ","));
    }

    int width = 0;
    int height = 0;
    for(int i = 0; i < entryCount; ++i)
    {
        if(lines[i].x1 > width) width = lines[i].x1;
        if(lines[i].y1 > height) height = lines[i].y1;
        if(lines[i].x2 > width) width = lines[i].x2;
        if(lines[i].y2 > height) height = lines[i].y2;
    }
    width++;
    height++;

    int *grid = malloc(sizeof(int) * width * height);
    memset(grid, 0, sizeof(int) * width * height);

    for(int i = 0; i < entryCount; ++i)
    {
        drawLine(lines[i], grid, width, height);
    }

    int intersections = 0;
    int maxIntersections = 0;
    for(int i = 0; i < width * height; ++i)
    {
        maxIntersections = max(maxIntersections, grid[i]);
        if(grid[i] > 1) intersections++;
    }

    char *image = malloc(sizeof(char) * width * height * 3);
    memset(image, 0, sizeof(char) * width * height * 3);

    for(int i = 0; i < width * height; ++i)
    {
        float interpolation = (float)grid[i] * (1.0f / (float)maxIntersections);
        char red = 0;
        char green = 0;
        char blue = 0;
        if(grid[i] != 0)
        {
            if(interpolation < 0.5f)
            {
                red = (char)ceil(((73.0f) * interpolation * 2));
                green = (char)ceil(((3.0f) * interpolation * 2));
                blue = (char)ceil(((124.0f) * interpolation * 2));
            }
            else
            {
                red = (char)ceil((255.0f - 73.0f) * ((interpolation - 0.5f) * 2) + 73.0f);
                blue = (char)ceil(-3.0f * ((interpolation - 0.5f) * 2) + 3.0f);
                blue = (char)ceil(-124.0f * ((interpolation - 0.5f) * 2) + 124.0f);
            }
            image[i * 3] = red;
            image[i * 3 + 1] = green;
            image[i * 3 + 2] = blue;
        }
    }

    stbi_write_bmp("map.bmp", width, height, 3, image);

    printf("%d\n", intersections);
    free(lines);
    free(grid);

    return 0;
}
