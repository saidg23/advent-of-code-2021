#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int strbin2i(const char* s) {
  register unsigned char *p = s;
  register unsigned int   r = 0;

  while (p && *p ) {
    r <<= 1;
    r += (unsigned int)((*p++) & 0x01);
  }

  return (int)r;
}

int countEntries(char *list, int length)
{
    int count = length;
    for(int i = 0; i < length; ++i)
    {
        if(strcmp(list + i * 13, "removed") == 0) count--;
    }
    return count;
}

int keepEntries(char *list, int length, char value, int pos)
{
    for(int i = 0; i < length; ++i)
    {
        if(strcmp(list + i * 13, "removed") == 0)
            continue;
        else
        {
            if(list[i * 13 + pos] != value)
                strcpy(list + i * 13, "removed");
        }
        if(countEntries(list, length) == 1)
        {
            return 1;
        }
    }
    return 0;
}

int main()
{
    FILE *input = fopen("input.txt", "r");
    if(input == NULL)
    {
        printf("Could not open file");
        exit(1);
    }

    char ch;
    int entryCount = 0;
    while((ch = fgetc(input)) != EOF)
    {
        if(ch == '\n') entryCount++;
    }
    rewind(input);

    const int maxLength = 13;
    char *oxygen = malloc(sizeof(char) * entryCount * maxLength);
    char *co2= malloc(sizeof(char) * entryCount * maxLength);

    char entry[50];
    int i = 0;
    int j = 0;
    while((ch = fgetc(input)) != EOF)
    {
        entry[i] = ch;
        if(ch == '\n')
        {
            if(j == 1000)
                continue;
            entry[i] = '\0';
            strcpy(oxygen + j * maxLength, entry);
            i = 0;
            j++;
            continue;
        }
        i++;
    }
    memcpy(co2, oxygen, sizeof(char) * entryCount * maxLength);

    int count0 = 0;
    int count1 = 0;
    i = 0;
    while(1)
    {
        for(int j = 0; j < entryCount; j++)
        {
            if(oxygen[j * maxLength + i] == '1') count1++;
            else if(oxygen[j * maxLength + i] == '0')count0++;
        }
        if(count1 > count0 || count1 == count0)
        {
            int ret = keepEntries(oxygen, entryCount, '1', i);
            if(ret) break;
        }
        else
        {
            int ret = keepEntries(oxygen, entryCount, '0', i);
            if(ret) break;
        }

        count1 = 0;
        count0 = 0;
        i++;
        if(i > 11) i = 0;
    }

    count0 = 0;
    count1 = 0;
    i = 0;
    while(1)
    {
        for(int j = 0; j < entryCount; j++)
        {
            if(co2[j * maxLength + i] == '1') count1++;
            else if(co2[j * maxLength + i] == '0')count0++;
        }
        if(count1 > count0 || count1 == count0)
        {
            int ret = keepEntries(co2, entryCount, '0', i);
            if(ret) break;
        }
        else
        {
            int ret = keepEntries(co2, entryCount, '1', i);
            if(ret) break;
        }

        count1 = 0;
        count0 = 0;
        i++;
        if(i > 11) i = 0;
    }

    int oxygenrating;
    for(int i = 0; i < entryCount; ++i)
    {
        if(strcmp(oxygen + i * maxLength, "removed") == 0) continue;
        printf("%s\n", oxygen + i * maxLength);
        oxygenrating = strbin2i(oxygen + i * maxLength);
    }

    int co2rating;
    for(int i = 0; i < entryCount; ++i)
    {
        if(strcmp(co2 + i * maxLength, "removed") == 0) continue;
        printf("%s\n", co2 + i * maxLength);
        co2rating = strbin2i(co2 + i * maxLength);
    }

    printf("%d\n", oxygenrating);
    printf("%d\n", co2rating);
    printf("%d\n", oxygenrating * co2rating);
    fclose(input);
    free(oxygen);
    free(co2);

    return 0;
}
