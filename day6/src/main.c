#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int getChildCound(int x, int days)
{
    return (days + (6 - x)) / 7;
}

long int getTotal(int x, int days)
{
    if(days <= 0)
        return 0;

    int totalChildren = getChildCound(x, days);
    long int acc = totalChildren;
    for(int i = 0; i < totalChildren; ++i)
        acc += getTotal(8, days - (x + (7 * i)) - 1);

    return acc;
}

int main()
{
    FILE *input = fopen("input.txt", "r");
    if(input == NULL)
    {
        printf("Could not open file\n");
        exit(1);
    }

    char ch;
    int entryCount = 0;
    while((ch = fgetc(input)) != EOF)
    {
        if(ch == ',') entryCount++;
    }
    entryCount++;
    rewind(input);

    int *initial = malloc(sizeof(int) * entryCount);
    int i = 0;
    while((ch = fgetc(input)) != EOF)
    {
        initial[i] = atoi(&ch);
        ch = fgetc(input);
        i++;
    }
    fclose(input);

    int counts[7] = {0};
    for(int i = 0; i < entryCount; ++i)
    {
        counts[initial[i]]++;
    }

    const int days = 80;

    long int acc = entryCount;
    for(int i = 0; i < 7; ++i)
    {
        if(counts[i])
            acc += counts[i] * getTotal(i, days);
    }

    printf("%ld\n", acc);
    free(initial);
    return 0;
}
